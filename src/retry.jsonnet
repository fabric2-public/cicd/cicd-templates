local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".retry": {
		stage: "retry",
    variables: {
			TEMPLATE_BRANCH: "main"
		},
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/python-toolbox:latest",
    script: |||
			wget https://gitlab.com/fabric2-public/cicd/cicd-templates/-/raw/${TEMPLATE_BRANCH}/src/scripts/retry/retry.py
			python retry.py
		|||,
		rules: [
			{
				"if": '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "trigger"',
				"when": "manual"
			},
		],
		needs: []
  },
};
boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)

