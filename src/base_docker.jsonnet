local boilerplate = importstr "lib/boilerplate.txt";
local dind = import "lib/dind.libsonnet";
local assume_role = importstr "scripts/awsauth/assume-role.sh";
local set_creds = importstr "scripts/awsauth/set-creds.sh";
local gcrane_copy = importstr "scripts/docker/gcrane-copy.sh";

local global_vars = {
  variables: {
    IMAGE: "$CI_REGISTRY_IMAGE:latest",
    DEPLOYMENT_ROLE: "arn:aws:iam::${AWS_ACCOUNT_ID}:role/${PIPELINE_ROLE}"
  },
};
local job_templates = {
  ".dind": dind.DindJob,
  ".deployment_script": {
    image: {
      name: "registry.gitlab.com/rafidsaad/images/aws-docker:latest",
      entrypoint: [""],
    },
    before_script: importstr "scripts/docker/deployment.sh"
  },
  ".build_and_push_gitlab": dind.DindJob {
    script: importstr "scripts/docker/build-and-push.sh"
  },
  ".docker_build_kaniko": dind.KanikoBuild{
    variables: {
      DOCKERFILE_PATH: "$CI_PROJECT_DIR/Dockerfile",
      DOCKER_TAG: "$CI_COMMIT_SHA",
      CONTEXT: "$CI_PROJECT_DIR",
      DOCKER_REGISTRY: "$CI_REGISTRY_IMAGE",
      DOCKER_IMAGE: "$DOCKER_REGISTRY:$DOCKER_TAG",
    },
    script: importstr "scripts/docker/kaniko-build.sh"
  },
  ".docker_push_gcrane": {
    image: {
      name: "amazon/aws-cli",
      entrypoint: [""],
    },    
    variables: {
      DOCKER_TAG: "$CI_COMMIT_SHA",
      AWS_REGION: "us-east-1",
      DOCKER_REGISTRY: "$CI_REGISTRY_IMAGE",
      DOCKER_IMAGE: "$DOCKER_REGISTRY:$DOCKER_TAG",
    },
    script: set_creds + assume_role + gcrane_copy
  },
};

local gitlabCiConf =  global_vars +
                      job_templates;

boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
