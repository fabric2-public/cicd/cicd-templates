local boilerplate = importstr "lib/boilerplate.txt";
local dind = import 'lib/dind.libsonnet';

local job_templates = {
  ".dependency-check": dind.DindJob {
    variables+: {
      "DC_VERSION": "latest",
      "DC_DIRECTORY": "$CI_PROJECT_DIR/OWASP-Dependency-Check",
      "DC_PROJECT": "dependency-check scan: $CI_PROJECT_NAME",
      "DATA_DIRECTORY": "$DC_DIRECTORY/data",
      "CACHE_DIRECTORY": "$DC_DIRECTORY/data/cache"    
    },
    cache: {
      paths: ["$DC_DIRECTORY/data"],
      key: "$CI_PROJECT_PATH_SLUG"
    },
    script: |||
      #!/bin/sh

      if [ ! -d "$DATA_DIRECTORY" ]; then
          echo "Initially creating persistent directory: $DATA_DIRECTORY"
          mkdir -p "$DATA_DIRECTORY"
      fi
      if [ ! -d "$CACHE_DIRECTORY" ]; then
          echo "Initially creating persistent directory: $CACHE_DIRECTORY"
          mkdir -p "$CACHE_DIRECTORY"
      fi

      # Make sure we are using the latest version
      docker pull owasp/dependency-check:$DC_VERSION

      docker run --rm \
          -e user=$USER \
          -u $(id -u ${USER}):$(id -g ${USER}) \
          --volume /var/run/docker.sock:/var/run/docker.sock \
          --volume ${CI_PROJECT_DIR}:/src:z \
          --volume "${DATA_DIRECTORY}":/usr/share/dependency-check/data:z \
          --volume ${CI_PROJECT_DIR}/odc-reports:/report:z \
          owasp/dependency-check:$DC_VERSION \
          --scan /src \
          --format "ALL" \
          --project "$DC_PROJECT" \
          --out /report
          # --suppression "/src/security/dependency-check-suppression.xml"
    |||,
    allow_failure: true,
    artifacts: {
      paths: ['$CI_PROJECT_DIR/odc-reports'],
      reports: {
        dependency_scanning: "$CI_PROJECT_DIR/odc-reports/dependency-check-report.json"
      },
      expire_in: "1 week"
    },
  }
};

boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)