local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".jmeter": {
    image: {
      name: "justb4/jmeter:latest",
      entrypoint: [""],
    },
    variables: {
      JMX_PATH: ""
    },
    script: [
      "mkdir testresults",
      "/entrypoint.sh -n -t $JMX_PATH -l ./testresults.log -e -o ./testresults"
    ],
    artifacts: {
      paths: ["testresults"]
    }
  }
};

local gitlabCiConf = job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)