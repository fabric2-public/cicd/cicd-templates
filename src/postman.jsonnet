local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".postman": {
    image: {
      name: "postman/newman_alpine33",
      entrypoint: [""],
    },
    cache: {},
    script: importstr "scripts/postman.sh",
    dependencies: [],
    artifacts: {
      reports: {
        junit: "report.xml"
      },
    },
  }
};

boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)
