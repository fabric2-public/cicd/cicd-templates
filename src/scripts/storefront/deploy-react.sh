#!/usr/bin/env bash

set -euxo pipefail

AWS_DEFAULT_REGION=${AWS_SELECTED_REGION:-"us-east-1"}
CLOUDFRONT_ID=${CLOUDFRONT_ID:-""}
PNI_DOCKERFILE_PREFIX=${PNI_DOCKERFILE_PREFIX:-""}
BUILD_ID_PREPEND_ENV_SLUG=${BUILD_ID_PREPEND_ENV_SLUG:-"1"}
STOREFRONT_ENVIRONMENT="${STOREFRONT_ENVIRONMENT:-"$CI_ENVIRONMENT_SLUG"}"

build_id="${BUILD_ID:-"$CI_COMMIT_SHORT_SHA"}"
if [[ -n ${PNI_DOCKERFILE_PREFIX} ]]; then
	build_id="${PNI_DOCKERFILE_PREFIX}-${STOREFRONT_ENVIRONMENT}"
fi
if [[ -n ${BUILD_ID_PREPEND_ENV_SLUG} ]]; then
	build_id="${STOREFRONT_ENVIRONMENT}-${build_id}"
fi

if [[ -n $PNI_DOCKERFILE_PREFIX ]]; then
	YQ_REF_FILE="${STOREFRONT_ENVIRONMENT}-${PNI_DOCKERFILE_PREFIX}-values.yaml"
	SF_NAME_REF="${STOREFRONT_NAME}-${PNI_DOCKERFILE_PREFIX}-${STOREFRONT_ENVIRONMENT}"
else
	YQ_REF_FILE="${STOREFRONT_ENVIRONMENT}-values.yaml"
	SF_NAME_REF="${STOREFRONT_NAME}-${STOREFRONT_ENVIRONMENT}"
fi

echo $build_id
export build_id
#echo "http://dl-4.alpinelinux.org/alpine/edge/community" >>/etc/apk/repositories
rm -rf /var/cache/apk/* && rm -rf /tmp/*
apk update
apk add git yq --no-cache
git clone https://storefront-deployments:$STOREFRONT_GITPASS@gitlab.com/fabric2/km/storefront/helm.git
git config user.email "ops@fabric.inc"
git config user.name "ops"
cd helm
git checkout main

yq eval '.deployment.tag = env(build_id)' ${STOREFRONT_NAME}/${YQ_REF_FILE} >>"${build_id}.yml"
mv "${build_id}.yml" "${STOREFRONT_NAME}/${YQ_REF_FILE}"
cat "${STOREFRONT_NAME}/${YQ_REF_FILE}"

if [[ ${AWS_DEFAULT_REGION} == "us-east-2" && ${STOREFRONT_ENVIRONMENT} == "production" ]]; then
	assume_role "201787177692"
	aws eks --region ${AWS_DEFAULT_REGION} update-kubeconfig --name ${EKS_CLUSTER_NAME_DR}
	CERT_ARN="$(aws --region "${AWS_DEFAULT_REGION}" secretsmanager get-secret-value --secret-id storefront/production/"${AWS_DEFAULT_REGION}"/secrets | jq -r '.SecretString' | jq -r --arg sclient "$STOREFRONT_REF" 'to_entries[] |select(.key == $sclient) |.value')"
	helm upgrade ${SF_NAME_REF} ${STOREFRONT_NAME} --install --values ${STOREFRONT_NAME}/${YQ_REF_FILE} --set ingress.certificate=${CERT_ARN} -n ${STOREFRONT_ENVIRONMENT}
else
	assume_role "${EKS_AWS_ACCOUNT_ID}"
	aws eks update-kubeconfig --name ${EKS_CLUSTER_NAME}
	helm upgrade ${SF_NAME_REF} ${STOREFRONT_NAME} --install --values ${STOREFRONT_NAME}/${YQ_REF_FILE} -n ${STOREFRONT_ENVIRONMENT}
fi

if [[ -z ${CLOUDFRONT_ID} ]]; then
	echo "No CF Distribution To Clean"
else
	echo "Clearing CloudFron Cache"
	aws cloudfront --region "${AWS_DEFAULT_REGION}" create-invalidation --distribution-id $CLOUDFRONT_ID --paths "/*"

fi

git config user.email "ops@fabric.inc"
git config user.name "ops"
CHANGES=$(git status --porcelain | wc -l)
if [[ ${CHANGES} -gt 0 ]]; then
	echo "Committing updated files to git"
	git add -A
	git commit -m "${STOREFRONT_NAME}-${STOREFRONT_ENVIRONMENT}:${build_id}"
	git push origin main
else
	echo "Nothing to commit"
fi
