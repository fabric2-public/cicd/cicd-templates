#!/usr/bin/env python

import json
import requests
import logging
import os

logging.basicConfig(
	format='%(asctime)s %(levelname)-6s %(message)s',
	level=logging.INFO,
	datefmt='%Y-%m-%d %H:%M:%S',
)

logger = logging.getLogger(__name__)

if 'LOG_LEVEL' in os.environ:
	logger.setLevel(os.environ['LOG_LEVEL'].upper())

for e in ['GITLAB_TRIGGER_TOKEN', 'CI_COMMIT_BRANCH', 'CI_PROJECT_ID']:
	if e not in os.environ or os.environ[e] == '':
		raise EnvironmentError('Required environment variable "%s" is not set or is empty.' % e)

trigger_url = 'https://gitlab.com/api/v4/projects/%s/trigger/pipeline' % os.environ['CI_PROJECT_ID']

body = {
	'token': os.environ['GITLAB_TRIGGER_TOKEN'],
	'ref': os.environ['CI_COMMIT_BRANCH'],
}

for key in os.environ:
	if 'TF_' in key:
		newkey = 'variables[%s]' % key
		body[newkey] = os.environ[key]

logger.debug(
	'sending request. trigger_url=%s request_body=%s' % 
	(trigger_url, json.dumps(body, indent=1))
)

response = requests.post(trigger_url, body)

try:
	logger.debug('response_json=%s' % response.json())
except ValueError as e:
	logger.warn('unable to json decode response. response=%s' % response)

logger.info('response_code=%s' % response.status_code)

if response.status_code not in [201, 200]:
	raise RuntimeError('unexpected response code: %s' % response.status_code)