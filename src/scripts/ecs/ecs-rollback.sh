if [[ -z ${AWS_ACCOUNT_ID-} ]]; then
    echo "ERROR: missing AWS_ACCOUNT_ID"
    exit 1
fi

assume_role "${AWS_ACCOUNT_ID}"

yum update -y && yum install jq -y
new_version=$(jq '.blue.task_definition' history.json)
previous_version=$(jq '.green.task_definition' history.json)
echo "Rollback from $new_version to $previous_version"
previous_version=$(jq '.green.task_definition' history.json | awk -F'/' '{print $2}' | sed 's/"//g')
aws ecs update-service --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME} --force-new-deployment --region ${AWS_REGION} --task-definition $previous_version
