assume_role "${AWS_ACCOUNT_ID}"
aws sts get-caller-identity

yum update -y && yum install jq -y
previous_version=$(jq '.green.task_definition' history.json)
new_version=$(jq '.blue.task_definition' history.json)
echo "Changing from $previous_version to $new_version"
new_version=$(jq '.blue.task_definition' history.json | awk -F'/' '{print $2}' | sed 's/"//g')
aws ecs update-service --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME} --force-new-deployment --region ${AWS_REGION} --task-definition $new_version
