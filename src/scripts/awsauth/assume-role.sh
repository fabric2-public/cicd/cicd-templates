echo "Initial role details: $(aws sts get-caller-identity)"

if [ -z "${AWS_REGION}" ]; then
  AWS_REGION="us-east-1"
fi

function assume_role() {
  echo "Role before assuming target role: $(aws sts get-caller-identity)"
  unset AWS_SESSION_TOKEN
  unset AWS_SECURITY_TOKEN
  export AWS_ACCESS_KEY_ID=$DEFAULT_AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY=$DEFAULT_AWS_SECRET_ACCESS_KEY
  account_id=$1
  echo "Role after creds reset: $(aws sts get-caller-identity)"
  CREDENTIALS=$(aws sts assume-role \
    --region "${AWS_REGION}" \
    --role-arn "arn:aws:iam::"${account_id}":role/"${PIPELINE_ROLE}"" \
    --role-session-name "Gitlab-${CI_JOB_ID}" \
    --query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
    --output text)
  read -r AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN <<<"$CREDENTIALS"
  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN
  aws configure set aws_access_key_id "${AWS_ACCESS_KEY_ID}"
  aws configure set aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
  aws configure set aws_session_token "${AWS_SESSION_TOKEN}"
  aws configure set region "${AWS_REGION}"
  aws configure set profile.default.role_arn "arn:aws:iam::${account_id}:role/"${PIPELINE_ROLE}""
  export AWS_SDK_LOAD_CONFIG="1"
  echo "Role after assuming target role: $(aws sts get-caller-identity)"
}
