export COPILOT_ACCOUNT_ID="752314070272"
export COPILOT_REGION="us-east-1"

function get_client_account_id {
  CREDENTIALS=$(aws sts assume-role \
    --region "${COPILOT_REGION}" \
    --role-arn "arn:aws:iam::${COPILOT_ACCOUNT_ID}:role/"${PIPELINE_ROLE}"" \
    --role-session-name "Gitlab-${CI_JOB_ID}" \
    --query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
    --output text)
  read -r accessKey secretKey sessionToken <<<"$CREDENTIALS"
  AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey AWS_SESSION_TOKEN=$sessionToken aws secretsmanager \
    get-secret-value \
    --region us-east-1 \
    --secret-id bitbucket/awsaccess/clients | jq '.SecretString | fromjson' >client-accounts-map.json
  accountId=$(jq -r --arg client "${CLIENT}" '.[$client]' client-accounts-map.json)
  echo $accountId
}
