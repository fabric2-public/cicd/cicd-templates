region=${REGION:-"us-east-1"}
compression_format="${ARTIFACT_COMPRESSION_FORMAT:-"zip"}"
build_path=${BUILD_PATH:-"${BUILD_ID}.${compression_format}"}

echo "Using ${compression_format} compression"

function repackage_warmup_plugin() {
	# Extract build artifact, fix the warmup plugin with the correct stage name,
	# and re-zip
	mkdir tmp
	unzip -qq build/warmUpPlugin.zip -d tmp
	sed -i'' 's/development/'$STAGE'/g' tmp/_warmup/index.js
	cd tmp
	zip -r ../build/warmUpPlugin.zip ./*
	cd ..
}

function extract_artifact_7z() {
	7zr -aoa e "${build_path}"
}

function extract_artifact_zip() {
	unzip -o "${build_path}"
}

function try_load_secrets() {

	# TODO(erstaples): remove this once sandbox/prod testing is complete
	# Gets the stage name from the "release" stage name: uat01release => uat01
	secret_stage="$(echo $STAGE | sed -r 's/(.+)release/\1/')"
	secret_name=${CLIENT}/${secret_stage}/secrets
	param_name=${secret_stage}_${PLATFORM}

	touch params.env.json secret.env.json

	aws \
		secretsmanager \
		get-secret-value \
		--region "$region" \
		--secret-id "${secret_name}" | jq -r '.SecretString | fromjson' >secret.env.json || echo "Secret ${secret_name} not found. Skipping..."

	aws \
		ssm \
		get-parameter \
		--region "$region" \
		--name "${param_name}" | jq -r '.Parameter.Value | fromjson' >params.env.json || echo "Param ${param_name} not found. Skipping..."

	echo >.env

	for json in params.env.json secret.env.json; do
		jq -r 'to_entries|map("export \(.key)=\"\(.value|tostring)\"")|.[]' "${json}" >>.env
	done

	# The secret may have its own $STAGE variable, so make sure we use ours
	export STAGE="$stage"
	source .env
}

check_deploy_window

package_name="${PACKAGE_NAME:-$CI_PROJECT_NAME}"
# shellcheck disable=SC2153
cd "${BUILD_DIR}"

stage="$STAGE"

GITLAB_REGISTRY_ARTIFACT_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${package_name}"
echo "GITLAB_REGISTRY_ARTIFACT_URL=$GITLAB_REGISTRY_ARTIFACT_URL"
echo "Preparing environment for ${CLIENT}"

# It's assumed that the get_client_account_id function will be in scope by the time this gets called. 
# The function is in scripts/awsauth/get-client-account-id.sh
account_id=$(get_client_account_id ${CLIENT})
assume_role "${account_id}"

echo "Downloading build artifact"
http_code=$(curl -LO -H "JOB-TOKEN:${CI_JOB_TOKEN}" -o/dev/null -w "%{http_code}" "${GITLAB_REGISTRY_ARTIFACT_URL}/${STAGE}/${build_path}")
echo "${http_code}"
if [ "${http_code}" -ne 200 ]; then
	echo "/${STAGE}/${build_path} not found; copying artifact to stage ${STAGE}"
	curl \
		-LO \
		-H "JOB-TOKEN:${CI_JOB_TOKEN}" \
		-o/dev/null \
		-w "%{http_code}" \
		"${GITLAB_REGISTRY_ARTIFACT_URL}/build/${build_path}"
fi
echo //registry.npmjs.org/:_authToken="${NPM_TOKEN}" >~/.npmrc
if [ ! -d node_modules ]; then npm install; fi

extract_artifact_${compression_format}
tar --overwrite -xzf build.tar.gz

# If there's a warmUpPlugin, make sure it's packaged with the correct stage name
stat build/warmUpPlugin.zip &>/dev/null && (repackage_warmup_plugin || true)

SKIP_SECRETS=${SKIP_SECRETS:-""}
if [[ -z $SKIP_SECRETS ]]; then
	try_load_secrets
fi

# Set Serverless Configuration using YQ utility in order to sync API keys between east1 and east2 regions
if [[ $region == "us-east-2" ]]; then
	if [[ -z $ORIG_API_NAME ]]; then
		echo "Environment variable ORIG_API_NAME not set! ORIG_API_NAME should be added in all repos."
		exit 1
	fi
	API_NAME="${STAGE}-${ORIG_API_NAME}"

	set +x
	if [[ ${PLATFORM} == "copilot" ]]; then
		if [[ ${STAGE} == "prod01" ]]; then
			# assume greatwall (copilot) production  role
			assume_role "212452576554"
		else
			# assume greatwall (copilot) sandbox role
			assume_role "177616441148"
		fi
		EAST_API_KEY_VALUE="$(aws apigateway get-api-keys --region us-east-1 --include-value | jq -r --arg target_api_key_name "$API_NAME" '.items[] | select(.name==$target_api_key_name).value')"
		yq eval --inplace '.provider.apiKeys=[{"name":"${self:provider.apiName}","value":"'${EAST_API_KEY_VALUE}'"}]' serverless.yml
		yq eval --inplace 'del(.provider.apiKeys)' serverless.yml
	else
		echo "Commerce deployments to us-east-2 are not supported. Please contact the Platform team for assistance."
		exit 1
	fi
fi

# `sls package` only works with a ".webpack" directory so rename it here
mv build .webpack
functionSet="${FUNCTION_SET:-"empty"}"
apiName="${API_NAME:-"empty"}"
apiHost="${SLS_API_HOST:-""}"
apiCert="${SLS_API_CERT:-""}"
options="--no-build --region ${region} --stage ${STAGE} --client ${CLIENT} --platform ${PLATFORM} --apiName ${apiName} --functionSet ${functionSet} --verbose"

if [[ -n $apiHost ]]; then
	options="${options} --api_host ${apiHost}"
fi

if [[ -n $apiCert ]]; then
	options="${options} --api_cert ${apiCert}"
fi

sls package $options
sls deploy $options