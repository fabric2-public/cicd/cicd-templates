#!/usr/bin/env bash

set -eux

compression_format="${ARTIFACT_COMPRESSION_FORMAT:-"zip"}"
build_path=${BUILD_PATH:-"${BUILD_ID}.${compression_format}"}
package_name="${PACKAGE_NAME:-$CI_PROJECT_NAME}"

echo "Using ${compression_format} compression"

function compress_zip() {
	zip -r "${build_path}" build.tar.gz
}

function compress_7z() {
	7zr a ${build_path} build.tar.gz
}

cd "${BUILD_DIR}"

GITLAB_REGISTRY_ARTIFACT_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${package_name}"
echo "GITLAB_REGISTRY_ARTIFACT_URL=$GITLAB_REGISTRY_ARTIFACT_URL"
http_code=$(curl -s -LI -H "JOB-TOKEN:${CI_JOB_TOKEN}" -o/dev/null -w "%{http_code}" "${GITLAB_REGISTRY_ARTIFACT_URL}"/"${STAGE}"/"${build_path}")
if [ "${http_code}" -eq 200 ]; then
	echo "artifact already exists; skipping build"
	exit 0
fi
echo //registry.npmjs.org/:_authToken="${NPM_TOKEN}" >~/.npmrc
if [ ! -d node_modules ]; then npm install; fi
aws configure set aws_access_key_id "${AWS_ACCESS_KEY_ID}"
aws configure set secret_access_key "${AWS_SECRET_ACCESS_KEY}"
CREDENTIALS=$(aws sts assume-role \
	--role-arn "arn:aws:iam::752314070272:role/Platform-GitlabPipelineAccessRole" \
	--role-session-name "MySession" \
	--query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
	--output text)
read -r AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN <<<"$CREDENTIALS"
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_SESSION_TOKEN
sls package --package build --client build --verbose
rm build/*.json
tar -czf build.tar.gz build serverless.yml

compress_${compression_format}

response_code=$(curl \
	-H "JOB-TOKEN:${CI_JOB_TOKEN}" \
	-X PUT \
	--silent \
	--data-binary "@${build_path}" \
	--write-out "%{http_code}" \
	--output /dev/null "${GITLAB_REGISTRY_ARTIFACT_URL}"/"${STAGE}"/"${build_path}")
if [ "${response_code}" -ne 201 ]; then
	echo "Error response code ${response_code}"
	exit 1
fi
