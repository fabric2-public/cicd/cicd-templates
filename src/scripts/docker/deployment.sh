#!/usr/bin/env bash

echo "===== Stage => ${STAGE_NAME//-/}, Account => ${AWS_ACCOUNT_ID}, Region => ${AWS_DEFAULT_REGION} ====="
echo "===== assuming permissions => ${DEPLOYMENT_ROLE} ====="
# shellcheck disable=SC2207

# set default creds
if [ -z "${DEFAULT_AWS_ACCESS_KEY_ID}" ]; then
    export DEFAULT_AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
fi

if [ -z "${DEFAULT_AWS_SECRET_ACCESS_KEY}" ]; then
    export DEFAULT_AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
fi

KST=($(aws sts assume-role --role-arn "arn:aws:iam::"${AWS_ACCOUNT_ID}":role/"${PIPELINE_ROLE}"" --role-session-name "${CI_PROJECT_NAME}-${GITLAB_USER_LOGIN}" --query '[Credentials.AccessKeyId,Credentials.SecretAccessKey,Credentials.SessionToken]' --output text))
unset AWS_SECURITY_TOKEN
export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
export AWS_ACCESS_KEY_ID=${KST[0]}
export AWS_SECRET_ACCESS_KEY=${KST[1]}
export AWS_SESSION_TOKEN=${KST[2]}
export AWS_SECURITY_TOKEN=${KST[2]}
echo "===== deploying to ${CI_ENVIRONMENT_NAME} environment ====="
