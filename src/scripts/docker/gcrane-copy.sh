#!/usr/bin/env bash
set -eux

yum install tar gzip -y

if [[ -z "${AWS_ACCOUNT_ID}" ]]; then
	echo "ERROR: missing AWS_ACCOUNT_ID"
	exit 1
fi

assume_role "${AWS_ACCOUNT_ID}"

curl -L https://github.com/google/go-containerregistry/releases/latest/download/go-containerregistry_Linux_x86_64.tar.gz -o go-containerregistry.tar.gz &&
	tar -zxvf go-containerregistry.tar.gz &&
	chmod +x gcrane &&
	mv gcrane /usr/local/bin/

aws ecr get-login-password --region ${AWS_REGION} | gcrane auth login ${ECR_URL} -u AWS --password-stdin
gcrane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

gcrane cp $DOCKER_IMAGE ${ECR_REPOSITORY}:$DOCKER_TAG
