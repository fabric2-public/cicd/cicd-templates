#!/usr/bin/env bash

build_environment="${BUILD_ENV:-$CI_ENVIRONMENT_NAME}"
deploy_s3_bucket="${S3_BUCKET:-$CI_ENVIRONMENT_NAME}"
name=${ARTIFACT_NAME:-"${CI_COMMIT_SHORT_SHA}.zip"}

if [[ -z ${deploy_s3_bucket-} ]]; then
	if [[ -z ${CI_ENVIRONMENT_URL-} ]]; then
		echo "ERROR: missing deploy_s3_bucket. No CI_ENVIRONMENT_URL to default to"
		exit 1
	fi
	echo "WARNING: missing deploy_s3_bucket; defaulting to CI_ENVIRONMENT_URL: $CI_ENVIRONMENT_URL"
	deploy_s3_bucket=$(echo "$CI_ENVIRONMENT_URL" | sed 's/https:\/\///')
fi
if [[ -z ${build_environment-} ]]; then
	if [[ -z ${CI_ENVIRONMENT_NAME-} ]]; then
		echo "ERROR: missing build_environment. No CI_ENVIRONMENT_NAME to default to"
		exit 1
	fi
	echo "WARNING: missing build environment; defaulting to CI_ENVIRONMENT_NAME: $CI_ENVIRONMENT_NAME"
	build_environment=$CI_ENVIRONMENT_NAME
fi

download_artifact "${build_environment}" "${name}"
unzip "${name}"
tar -xzvf "${build_environment}.tar.gz"

# get_client_account_id and assume_role are defined in src/scripts/awsauth and are assumed to be in scope
account_id=$(get_client_account_id "${CLIENT}")
assume_role "${account_id}"

aws s3 sync artifact-contents "s3://${deploy_s3_bucket}" --exact-timestamps --delete --acl public-read

echo "Updated ${deploy_s3_bucket} with artifact ${name}"
