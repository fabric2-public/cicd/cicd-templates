#!/usr/bin/env bash

set -eux

if [[ -z ${source_env_bucket-} ]]; then
	echo "ERROR: missing required source_env_bucket"
	exit 1
fi
if [[ -z ${target_env_bucket-} ]]; then
	echo "ERROR: missing required target_env_bucket"
	exit 1
fi

export artifact_file=${BUILD_ID}.zip
source_artifact_s3_url=s3://$source_env_bucket/$CI_PROJECT_NAME/$artifact_file
target_artifact_s3_url=s3://$target_env_bucket/$CI_PROJECT_NAME/$artifact_file
echo "source_artifact_s3_url $source_artifact_s3_url"
echo "target_artifact_s3_url $target_artifact_s3_url"
aws s3 cp "$source_artifact_s3_url" "$target_artifact_s3_url"
echo "Copied artifact $artifact_file from $source_env_bucket to $target_artifact_s3_url"
