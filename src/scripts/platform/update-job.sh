#!/usr/bin/env bash
set -eux

if [[ -z ${PLATFORM_DISABLE_HISTORY} ]]; then

	cat <<EOF >post.json
{
  "repo": "${CI_PROJECT_NAME}",
  "client": "${CLIENT}",
  "buildNumber": "${CI_JOB_ID}", 
  "buildStatus": "SUCCESS"
}
EOF
	curl \
		--silent \
		--location \
		--request POST \
		--header "x-api-key:${PLATFORM_API_KEY}" \
		--header "Content-Type:application/json" \
		--data-binary @post.json "${PLATFORM_BASE_URL}"/updateBuild
fi
