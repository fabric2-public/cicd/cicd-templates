#!/usr/bin/env bash

set -x

if [ -n "$TF_VARS_FILE_PATH" ]; then
	if [ -n "$TF_TARGET" ]; then
		terraform plan -out=$PLAN -var-file="$TF_VARS_FILE_PATH" -target="$TF_TARGET"
	else
		terraform plan -out=$PLAN -var-file="$TF_VARS_FILE_PATH"
	fi
else
	terraform plan -out=$PLAN
fi
