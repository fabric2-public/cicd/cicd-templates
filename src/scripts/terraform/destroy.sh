#!/usr/bin/env bash

set -x

if [ -n "$TF_VARS_FILE_PATH" ]; then
	if [ -n "$TF_TARGET" ]; then
		terraform destroy -auto-approve -var-file="$TF_VARS_FILE_PATH" -target="$TF_TARGET"
	else
		terraform destroy -auto-approve -var-file="$TF_VARS_FILE_PATH"
	fi
else
	terraform destroy -auto-approve
fi

if [ -z "${PRESERVE_TF_STATE_ON_DELETE:-""}" ]; then
	state_name=${TF_STATE_NAME//./-}
	echo "Deleting state $state_name"
	curl -X DELETE -H "PRIVATE-TOKEN:$TF_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/terraform/state/$state_name"
fi
