local boilerplate = importstr "lib/boilerplate.txt";

local job_templates = {
	".build-beanstalkartifacts": {
		image: {
			name: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/php-toolbox:latest"
		},
		script: |||
			#!/usr/bin/env bash
			
			set -eux

			rm /etc/apt/sources.list.d/additional.list
			apt-get update
			apt-get install -y zip
			zip application.zip -r * .[^.]*
		|||
	},
		
    ".deploy-beanstalkartifacts": {
	    variables: {
           build_environment: "$CI_ENVIRONMENT_NAME"
        },
        image: {
           name: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/python-toolbox:v0.0.1",
        },
		script: importstr "scripts/beanstalk-deploy.sh"
  }
};

local gitlabCiConf = job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)