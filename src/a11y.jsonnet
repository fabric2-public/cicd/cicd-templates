
local boilerplate = importstr "lib/boilerplate.txt";
local image = {};
local stages = {};
local global_vars = {};

local job_templates = {
  ".a11y": {
    image: "registry.gitlab.com/gitlab-org/ci-cd/accessibility:5.3.0-gitlab.3",
    variables: {
      "a11y_url": ""
    },
    script: "/gitlab-accessibility.sh $a11y_url",
    artifacts: {
      when: "always",
      expose_as: "Accessibility Reports",
      paths: ["reports/"],
      reports: {
        accessibility: "reports/gl-accessibility.json"
      }
    },
    needs: []
  },
};

local gitlabCiConf =  image +
                      stages +
                      global_vars +
                      job_templates;

local yaml = std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true);
boilerplate + yaml