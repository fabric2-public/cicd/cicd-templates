local boilerplate = importstr "lib/boilerplate.txt";
local assume_role = importstr "scripts/awsauth/assume-role.sh";
local set_creds = importstr "scripts/awsauth/set-creds.sh";
local ecs_deploy = importstr "scripts/ecs/ecs-deploy.sh";

local job_templates = {
  ".ecs-new-task-definition": {
    image: "registry.gitlab.com/fabric2/km/xm/gitlab-templates/python-toolbox:latest",
    script: "python3 ecs-task-definition.py",
    variables: {
      CLUSTER_NAME: "CLUSTER",
      SERVICE_NAME: "SERVICE_NAME",
      DOCKER_IMAGE: "DOCKER_IMAGE",
      AWS_REGION: "us-east-1",
    },
    artifacts: {
      paths: [
        "history.json"
      ],
    },
  },
  ".ecs-deploy": {
    image: "amazon/aws-cli",
    variables: {
      CLUSTER_NAME: "CLUSTER",
      SERVICE_NAME: "SERVICE_NAME",
      AWS_REGION: "us-east-1"
    },
    artifacts:{
      paths: [
        "history.json"
      ]
    },
    script: set_creds + assume_role + ecs_deploy,
  },
  ".ecs-rollback": {
    image: "amazon/aws-cli",
    variables: {
      CLUSTER_NAME: "CLUSTER",
      SERVICE_NAME: "SERVICE_NAME",
      AWS_REGION: "us-east-1"
    },
    artifacts:{
      paths: [
        "history.json"
      ]
    },
    script: importstr "scripts/ecs/ecs-rollback.sh",
  },
  ".ecs-native": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/python-toolbox:latest",
    script: "ecs",
    artifacts: {
      paths: [
        "history.json"
      ],
    },
  },
};
local config = job_templates;
boilerplate + std.manifestYamlDoc(config, indent_array_in_object=true)
