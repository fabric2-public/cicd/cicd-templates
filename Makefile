.PHONY: fmt-write fmt-check shellcheck templates clean release-branch release 

S3_BUCKET_URL ?= $(FABRIC_GITLAB_CI_TEMPLATE_BUCKET_URL)
VERSION ?= $(FABRIC_GITLAB_CI_TEMPLATE_VERSION)
BRANCH ?= $(CI_COMMIT_REF_NAME)
TEMPLATES_DIR = templates

all: shellcheck fmt-check $(TEMPLATES_DIR)

local: shellcheck fmt $(TEMPLATES_DIR)

templates:
	./build.sh $(TEMPLATES_DIR)

shellcheck:
	shellcheck src/scripts/**/*.sh

fmt:
	shfmt -s -w src/scripts

fmtcheck:
	shfmt -s -w -d src/scripts

clean:
	rm -rf $(OUT_DIR)
	find templates -depth 1 -name "*.yml" -delete