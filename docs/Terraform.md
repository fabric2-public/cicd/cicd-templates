# Terraform Pipeline

The terraform templates cover the terraform environment lifecycle end-to-end including planning, applying, and destorying terraform state.

## State Store

Terraform state is stored in Gitlab using the `http` Terraform state store. Add the following to your `main.tf` to configure the http backend. The `terraform init` command will take care of the rest.

main.tf:

```hcl
terraform {
  backend "http" {
  }
}
```

## Project Structure

Structure your terraform project so that the environment-scoped tfvars files live in `env` directory. The name of a tfvar file should match the `environment:name` in the gitlab-ci.yml (See https://docs.gitlab.com/ee/ci/environments/ for more information about Gitlab environments). For example, the following project has three environments: development, review, and production.

```
src
├── <application code>
|
terraform
├── env
│   ├── development.tfvars
│   ├── production.tfvars
│   └── review.tfvars
├── main.tf
├── outputs.tf
├── s3.tf
└── variables.tf
```

## Build Variables

Secrets, keys, and other sensitive data that is kept outside of the source code can be included through `TF_VAR_<name>` variables. For instance, to include an api key in the terraform plan you could add the following to the .gitlab-ci.yml and refer to it within the terraform as `var.my_api_key`:

```yaml
variables:
  TF_VAR_my_api_key: $API_KEY
```

## Templates

### .terraform-init

Runs `terraform init`.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| `TF_ROOT` | The directory in which to run the terraform command | yes | |
| `TF_STATE_NAME` | The name to use for the terraform state. | yes | |
| `TF_ACCESS_TOKEN` | A project or personal access token with `api` scope. | yes | |

### .terraform-plan

Runs `terraform plan` and saves the output as a report artifact, which is viewable in Gitlab merge requests.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| `TF_ROOT` | The directory in which to run the terraform command | yes | |
| `tf_vars_file_name` | The name of the terraform vars file to use | no | `$CI_ENVIRONMENT_NAME` |

### .terraform-apply

Runs `terraform apply` on the plan created perviously.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| `TF_ROOT` | The directory in which to run the terraform command | yes | |
| `tf_vars_file_name` | The name of the terraform vars file to use | no | `$CI_ENVIRONMENT_NAME` |

### .terraform-destroy

Runs `terraform destroy` and deletes the workspace associated with the environment.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| `TF_ROOT` | The directory in which to run the terraform command | yes | |
| `tf_vars_file_name` | The vars file to use | no | `$CI_ENVIRONMENT_NAME` |

## Example Usage

Given the project structure above, you could create a .gitlab-ci.yml file with the following configuration. This example would only deploy the _development_ environment. This configuration could be duplicated (or better yet, made into a template and reused) for the _review_ and _production_ environments.

```yaml
include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/terraform.yml

variables:
  TF_ROOT: terraform
  TF_ACCESS_TOKEN: <store this in the cicd variables>
  TF_STATE_NAME: development

stages:
  - terraform-init
  - terraform-plan
  - terraform-apply

terraform-init:
  stage: terraform-init
  extends: .terraform-init
  environment:
    name: development

terraform-plan:
  stage: terraform-plan
  extends: .terraform-plan  
  needs: 
    - terraform-init
  environment:
    name: development
  variables:
    TF_VAR_my_api_key: "$API_KEY"

terraform-apply:
  stage: terraform-apply
  extends: .terraform-apply
  needs: 
    - terraform-plan
  environment:
    name: development
```
