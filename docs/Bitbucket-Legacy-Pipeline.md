# Bitbucket Legacy Pipeline

Deploying the shared repos in Gitlab is no different than deploying them from Bitbucket. You will be using the same form, deployment scripts, and serverless stack. There is now an approval step for deployments to sandbox and production. Authorized users are configured in the settings->CICD->protected environments section.

## Trigger Pipeline

1. Go to CI/CD -> Pipelines
2. Click "Run Pipeline" in the top right corner.
3. Select a branch, fill out the form fields, and click "Run Pipeline"
