# Storefront EKS Pipeline

## Setup

The only required variable to set is `STOREFRONT_NAME`. The yaml below is all you need to get the pipeline up and running.

```yaml
include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/pipelines/storefront.yml

variables:
  STOREFRONT_NAME: <name-of-storefront>
```

## Docker Images

Docker images are stored in Gitlab in the respective project's Container Registry: `https://gitlab.com/<project-path>/-/container_registry`.
