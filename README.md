# ci-templates

## Dependencies

1. Overcommit:
Run `sudo gem install overcommit`
2. Jsonnet:
Run `brew install jsonnet`
3. shellcheck:
Run `brew install shellcheck`
4. shfmt:
Run `brew install shfmt`

## Build

1. Run `make local` to run formatting checks and compile jsonnet templates.

CICD templates are compiled via Jsonnet. Once the dependencies are installed you can run the Makefile to build the templates.

[Docs](https://gitlab.com/fabric2-public/cicd/cicd-templates/-/tree/main/docs)
