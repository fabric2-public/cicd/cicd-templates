include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/npm.yml
      - templates/sonarcloud.yml

image: bitnami/node:14

stages:
  - install-dependencies
  - check-commits
  - verify-packages
  - lint-prettier
  - build-dev
  - test
  - analyze
  - deploy-dev
  - rollback-dev
  - integration-test
  - build-staging
  - deploy-staging
  - rollback-staging
  - build-prod02
  - deploy-prod02
  - rollback-prod02
  - build-sandbox
  - deploy-sandbox
  - release-sandbox
  - rollback-sandbox
  - build-prod
  - deploy-production
  - release-production
  - rollback-production

variables:
  RELEASE_LOWER_ENV: "$CI_COMMIT_SHORT_SHA"
  RELEASE_HIGH_ENV: "$CI_COMMIT_TAG"

.rules-anchor:
  rules:
    - &ci_rules
      if: "$CI_MERGE_REQUEST_ID"
    - &main
      if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
    - &release_lower_environment_auto
      if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
    - &release_lower_environment_manual
      if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: manual
    - &release_high_environment_auto
      if: $CI_COMMIT_TAG
    - &release_high_environment_manual
      if: $CI_COMMIT_TAG
      when: manual

.base-rollback:
  variables:
    BUCKET: origin
    FOLDER: $CI_PROJECT_NAME
    VERSION: $CI_COMMIT_SHORT_SHA
  script: |
    echo "VERSION: $VERSION"
    set -eo pipefail
    shopt -s nullglob
    echo s3://$BUCKET/$CI_PROJECT_NAME
    previous_commit=$(aws s3 ls s3://$BUCKET/$CI_PROJECT_NAME/PREVIOUS_BUILDS/ --recursive | awk '{print $1,$2,$4}' | uniq -c | sort -nr | awk -F '/' '{ if(length($3) == 8 || $2 !~ /\./) print($3) }' | uniq -c | awk 'NR==2{ print $2}')
    echo "Last Commit:" $previous_commit
    echo "------"
    echo "Reverting from $VERSION to $previous_commit"
    echo "------"
    echo "Rollback to previous versions"
    aws s3 cp s3://$BUCKET/$CI_PROJECT_NAME/PREVIOUS_BUILDS/$previous_commit s3://$BUCKET/$CI_PROJECT_NAME/ --recursive
    echo "------"
    echo "------"
    echo "Running purge change in cloudfront $CLOUDFRONT_ID"
    aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_ID --paths "/$CI_PROJECT_NAME/*"
    echo "------"
    echo "------"

# DEVELOPMENT AND TEST STAGES

.base-dev:
  stage: deploy-dev
  environment:
    name: development
    url: $DEPLOY_DEV_URL
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

install-dependencies:
  stage: install-dependencies
  script: |
    echo "Compiling the code..."
    echo '//registry.npmjs.org/:_authToken='"$NPM_TOKEN" > .npmrc
    npm ci
  artifacts:
    paths:
      - node_modules/
    expire_in: 10 days
  rules:
    - *ci_rules
    - *release_lower_environment_auto
    - *release_high_environment_auto

check-commits:
  stage: check-commits
  needs: ["install-dependencies"]
  script: |
    echo Counting commits from branch $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/fabric2/km/copilot/mfa/${CI_PROJECT_NAME}.git
    cd $CI_PROJECT_NAME
    git checkout $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    COUNTS=$(env git rev-list --count HEAD ^$CI_DEFAULT_BRANCH)
    echo "Your MR have $COUNTS commit(s)"
    if [ "$COUNTS" != "1" ]; then
      echo "Your MR has more than one commit. Please, squash your commits before."
      exit 1;
    fi
  rules:
    - *ci_rules

verify-packages:
  stage: verify-packages
  needs: ["install-dependencies", "check-commits"]
  script: |
    echo '//registry.npmjs.org/:_authToken='"$NPM_TOKEN" > .npmrc

    if [ "$CI_PROJECT_NAME" != "copilot-mfa-communication" ]; then
      echo "Getting the current CoPilot-UI Version"
      COPILOT_UI_CV=$(npm view @teamfabric/copilot-ui version | awk '{ split($1,a,"."); VERSION=a[1]"."a[2]; print VERSION }')
      COPILOT_UI_LV=$(echo "console.log(require('@teamfabric/copilot-ui/package.json').version);" | node | awk '{ split($1,a,"."); VERSION=a[1]"."a[2]; print VERSION }')

      echo "Registry Version: $COPILOT_UI_CV"
      echo "Local Version: $COPILOT_UI_LV" 

      if [ "$COPILOT_UI_LV" == "$COPILOT_UI_CV" ]; then
        echo "Package is up to date!";
      else 
        echo "Your CoPilot-UI package is not up to date! Please, check the version."
        exit 1;
      fi
    fi

    echo "Getting the current Styleguide Version"
    STYLEGUIDE_CV=$(npm view @teamfabric/styleguide version | awk '{ split($1,a,"."); VERSION=a[1]"."a[2]; print VERSION }')
    STYLEGUIDE_LV=$(echo "console.log(require('@teamfabric/styleguide/package.json').version);" | node | awk '{ split($1,a,"."); VERSION=a[1]"."a[2]; print VERSION }')

    echo "Registry Version: $STYLEGUIDE_CV"
    echo "Local Version: $STYLEGUIDE_LV" 

    if [ "$STYLEGUIDE_LV" == "$STYLEGUIDE_CV" ]; then
      echo "Package is up to date!";
    else 
      echo "Your Styleguide package is not up to date! Please, check the version."
      exit 1;
    fi

    echo "Getting the current Eslint Version"
    ESLINT_CV=7.3
    ESLINT_LV=$(echo "console.log(require('eslint/package.json').version);" | node | awk '{ split($1,a,"."); VERSION=a[1]"."substr(a[2],1,1); print VERSION }')

    echo "Registry Version: $ESLINT_CV"
    echo "Local Version: $ESLINT_LV" 

    if [ "$ESLINT_LV" == "$ESLINT_CV" ]; then
      echo "Package is up to date!";
    else 
      echo "Your Eslint package is not up to date! Please, check the version."
      exit 1;
    fi

    echo "Getting the current Prettier Version"
    PRETTIER_CV=2.2
    PRETTIER_LV=$(echo "console.log(require('prettier/package.json').version);" | node | awk '{ split($1,a,"."); VERSION=a[1]"."a[2]; print VERSION }')

    echo "Registry Version: $PRETTIER_CV"
    echo "Local Version: $PRETTIER_LV" 

    if [ "$PRETTIER_LV" == "$PRETTIER_CV" ]; then
      echo "Package is up to date!";
    else 
      echo "Your Prettier package is not up to date! Please, check the version."
      exit 1;
    fi
  rules:
    - *ci_rules

lint-prettier:
  stage: lint-prettier
  needs: ["install-dependencies", "verify-packages"]
  script: |
    echo "Set styles files"
    cp -a node_modules/@teamfabric/styleguide/dist/styles/. .

    echo "Lint the commit"
    npx eslint $(git diff --name-only HEAD^ | grep -E '\.(tsx|ts|js|jsx)$' | xargs)

    echo "Now, prettier"    
    npx prettier --check $(git diff --name-only HEAD^ | grep -E '\.(tsx|ts|js|jsx)$' | xargs)
  rules:
    - *ci_rules

build-dev:
  stage: build-dev
  needs: ["install-dependencies"]
  script: |
    export NODE_ENV=development

    if test -f "dev.env"; then
      cp dev.env .env
    fi

    echo "Building the application"
    npm run build

    if test -f ".env"; then
      rm .env
    fi

    echo "Build complete."
  artifacts:
    paths:
      - ./dist
    expire_in: 10 days
  rules:
    - *ci_rules
    - *release_lower_environment_auto
    - if: $CI_COMMIT_TAG
      when: never

test:
  stage: test
  dependencies:
    - build-dev
  extends:
    - .npm-run
  variables:
    npm_cmd: coverage
  artifacts:
    paths:
      - ./coverage
    expire_in: 10 days
  rules:
    - *ci_rules
    - *main

analyze:
  stage: analyze
  extends: .sonarcloud
  rules:
    - *ci_rules
    - *main

deploy-dev:
  extends: .base-dev
  script: |

    if [ "$CI_PROJECT_NAME" == "copilot-mfa-root" ]; then
      aws s3 cp ./dist/index.html s3://$BUCKET_DEV/
      aws s3 cp ./dist/favicon.ico s3://$BUCKET_DEV/
    fi

    aws s3 sync ./dist/ s3://$BUCKET_DEV/$BUCKET_FOLDER/PREVIOUS_BUILDS/$CI_COMMIT_SHORT_SHA/
    aws s3 sync ./dist/ s3://$BUCKET_DEV/$BUCKET_FOLDER/
    echo "invalidating the cache from mfa aws account"
    aws cloudfront create-invalidation --distribution-id $MFA_DEV02_CLOUDFRONT_DIST_ID --paths "/*"
    echo "switching to internal_non_production aws account"
    export AWS_ACCESS_KEY_ID=$INP_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$INP_AWS_SECRET_ACCESS_KEY
    echo "invalidating the cache from dev02 cloudfront distribution"
    aws cloudfront create-invalidation --distribution-id $INP_DEV02_CLOUDFRONT_DIST_ID --paths "/*"
  rules:
    - *release_lower_environment_auto

rollback-dev:
  extends: .base-dev
  variables:
    BUCKET: $BUCKET_DEV
    FOLDER: $BUCKET_FOLDER
    VERSION: $CI_COMMIT_SHORT_SHA
  needs: ["deploy-dev"]
  script: !reference [.base-rollback, script]
  rules:
    - *release_lower_environment_manual

integration-test:
  stage: integration-test
  trigger:
    include:
      - project: fabric2-public/cicd/cicd-templates
        ref: main
        file: examples/mfa/config/integration-test.yaml
  needs: ["build-dev", "deploy-dev"]
  rules:
    - *main

# STAGING STAGE

.base-staging:
  stage: deploy-staging
  environment:
    name: staging
    url: $DEPLOY_STAGING_URL
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

build-staging:
  stage: build-staging
  script: |
    if test -f "stg02.env"
    then
      cp stg02.env .env
      npm run build
    else
      echo "No .env file found, skipping build"
    fi

    if test -f ".env"; then
      rm .env
    fi

    echo "Build complete."
  artifacts:
    paths:
      - ./dist
    expire_in: 10 days
  needs: ["install-dependencies", "build-dev", "deploy-dev"]
  rules:
    - *release_lower_environment_auto

deploy-staging:
  extends: .base-staging
  needs: ["build-staging"]
  script: |

    if [ "$CI_PROJECT_NAME" == "copilot-mfa-root" ]; then
      aws s3 cp ./dist/index.html s3://$BUCKET_STG/
      aws s3 cp ./dist/favicon.ico s3://$BUCKET_STG/
    fi

    aws s3 sync ./dist/ s3://$BUCKET_STG/$BUCKET_FOLDER/PREVIOUS_BUILDS/$CI_COMMIT_SHORT_SHA/
    aws s3 sync ./dist/ s3://$BUCKET_STG/$BUCKET_FOLDER/
    echo "invalidating the cache from mfa aws account"
    aws cloudfront create-invalidation --distribution-id $MFA_STG02_CLOUDFRONT_DIST_ID --paths "/*"
    echo "switching to internal_non_production aws account"
    export AWS_ACCESS_KEY_ID=$INP_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$INP_AWS_SECRET_ACCESS_KEY
    echo "invalidating the cache from stg02 cloudfront distribution"
    aws cloudfront create-invalidation --distribution-id $INP_STG02_CLOUDFRONT_DIST_ID --paths "/*"
  rules:
    - *release_lower_environment_manual

rollback-staging:
  extends: .base-staging
  variables:
    BUCKET: $BUCKET_STG
    FOLDER: $BUCKET_FOLDER
    VERSION: $CI_COMMIT_SHORT_SHA
  needs: ["deploy-staging"]
  script: !reference [.base-rollback, script]
  rules:
    - *release_lower_environment_manual

#PROD02 STAGE

.base-prod02:
  stage: deploy-prod02
  environment:
    name: prod02
    url: $DEPLOY_PROD02_URL
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

build-prod02:
  stage: build-prod02
  needs: ["install-dependencies"]
  script: |
    if test -f "prod02.env"; then
      cp prod02.env .env
    fi

    echo "Building the application"
    npm run build

    if test -f ".env"; then
      rm .env
    fi

    echo "Build complete."
  artifacts:
    paths:
      - ./dist
    expire_in: 30 days
  rules:
    - *release_high_environment_auto

deploy-prod02:
  extends: .base-prod02
  needs: ["build-prod02"]
  script: |

    if [ "$CI_PROJECT_NAME" == "copilot-mfa-root" ]; then
      aws s3 cp ./dist/index.html s3://$BUCKET_PROD02/
      aws s3 cp ./dist/favicon.ico s3://$BUCKET_PROD02/
    fi

    aws s3 sync ./dist/ s3://$BUCKET_PROD02/$CI_PROJECT_NAME/PREVIOUS_BUILDS/$RELEASE_HIGH_ENV/
    aws s3 sync ./dist/ s3://$BUCKET_PROD02/$CI_PROJECT_NAME/
    echo "invalidating the cache from mfa aws account"
    aws cloudfront create-invalidation --distribution-id $MFA_PROD02_CLOUDFRONT_DIST_ID --paths "/*"
    echo "switching to internal_production aws account"
    export AWS_ACCESS_KEY_ID=$IN_PROD_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$IN_PROD_AWS_SECRET_ACCESS_KEY
    echo "invalidating the cache from prod02 cloudfront distribution"
    aws cloudfront create-invalidation --distribution-id $IN_PROD_PROD02_CLOUDFRONT_DIST_ID --paths "/*"
  when: manual
  rules:
    - *release_high_environment_manual

rollback-prod02:
  extends: .base-prod02
  needs: ["deploy-prod02"]
  variables:
    BUCKET: $BUCKET_PROD02
    FOLDER: $BUCKET_FOLDER
    VERSION: $CI_COMMIT_TAG
  script: !reference [.base-rollback, script]
  rules:
    - *release_high_environment_manual

# SANDBOX STAGE

.base-sandbox:
  stage: deploy-sandbox
  environment:
    name: sandbox
    url: $DEPLOY_SANDBOX_URL
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

build-sandbox:
  stage: build-sandbox
  needs: ["build-prod02"]
  script: |
    if test -f "sandbox.env"
    then
      cp sandbox.env .env
      npm run build
    else
      echo "No .env file found, skipping build"
    fi

    if test -f ".env"; then
      rm .env
    fi

    echo "Build complete."
  artifacts:
    paths:
      - node_modules/
      - ./dist
    expire_in: 30 days
  rules:
    - *release_high_environment_auto

deploy-sandbox:
  extends: .base-sandbox
  needs: ["build-sandbox", "deploy-prod02"]
  variables:
    MFA_ENV: sandbox
  script: |
    if [ "$CI_PROJECT_NAME" == "copilot-mfa-root" ]; then
      aws s3 cp ./dist/index.html s3://$BUCKET_SANDBOX/
      aws s3 cp ./dist/favicon.ico s3://$BUCKET_SANDBOX/
    fi

    aws s3 sync ./dist/ s3://$BUCKET_SANDBOX/$CI_PROJECT_NAME/PREVIOUS_BUILDS/$RELEASE_HIGH_ENV/
    echo "invalidating the cache from mfa aws account"
    aws cloudfront create-invalidation --distribution-id $MFA_SANDBOX_CLOUDFRONT_DIST_ID --paths "/*"
  rules:
    - *release_high_environment_manual

release-sandbox:
  extends: .base-sandbox
  needs: ["deploy-sandbox"]
  script: |
    aws s3 cp s3://$BUCKET_SANDBOX/$CI_PROJECT_NAME/PREVIOUS_BUILDS/$RELEASE_HIGH_ENV/ s3://$BUCKET_SANDBOX/$CI_PROJECT_NAME/ --recursive
    aws cloudfront create-invalidation --distribution-id $MFA_SANDBOX_CLOUDFRONT_DIST_ID --paths "/*"
  rules:
    - *release_high_environment_manual

rollback-sandbox:
  extends: .base-sandbox
  needs: ["release-sandbox"]
  variables:
    BUCKET: $BUCKET_SANDBOX
    FOLDER: $BUCKET_FOLDER
    VERSION: $CI_COMMIT_TAG
    CLOUDFRONT_ID: $MFA_SANDBOX_CLOUDFRONT_DIST_ID
  script: !reference [.base-rollback, script]
  rules:
    - *release_high_environment_manual

# PRODUCTION STAGE

.base-prod:
  stage: deploy-production
  environment:
    name: production
    url: $DEPLOY_PRODUCTION_URL
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

build-prod:
  stage: build-prod
  needs: ["build-sandbox"]
  script: |
    if test -f "prod.env"
    then
      cp prod.env .env
      npm run build
    else
      echo "No .env file found, skipping build"
    fi

    if test -f ".env"; then
      rm .env
    fi

    echo "Build complete."
  artifacts:
    paths:
      - node_modules/
      - ./dist
    expire_in: 30 days
  rules:
    - *release_high_environment_auto

deploy-production:
  extends: .base-prod
  needs: ["build-prod", "release-sandbox"]
  variables:
    MFA_ENV: production
  script: |
    if [ "$CI_PROJECT_NAME" == "copilot-mfa-root" ]; then
      aws s3 cp ./dist/index.html s3://$BUCKET_PRODUCTION/
      aws s3 cp ./dist/favicon.ico s3://$BUCKET_PRODUCTION/
    fi

    aws s3 sync ./dist/ s3://$BUCKET_PRODUCTION/$CI_PROJECT_NAME/PREVIOUS_BUILDS/$RELEASE_HIGH_ENV/
    echo "invalidating the cache from mfa aws account"
    aws cloudfront create-invalidation --distribution-id $MFA_PRODUCTION_CLOUDFRONT_DIST_ID --paths "/*"
  rules:
    - *release_high_environment_manual

release-production:
  extends: .base-prod
  needs: ["deploy-production"]
  script: |
    aws s3 cp s3://$BUCKET_PRODUCTION/$CI_PROJECT_NAME/PREVIOUS_BUILDS/$RELEASE_HIGH_ENV/ s3://$BUCKET_PRODUCTION/$CI_PROJECT_NAME/ --recursive
    echo "invalidating the cache from mfa aws account"
    aws cloudfront create-invalidation --distribution-id $MFA_PRODUCTION_CLOUDFRONT_DIST_ID --paths "/*"
  rules:
    - *release_high_environment_manual

rollback-production:
  extends: .base-prod
  needs: ["release-production"]
  variables:
    BUCKET: $BUCKET_PRODUCTION
    FOLDER: $BUCKET_FOLDER
    VERSION: $CI_COMMIT_TAG
    CLOUDFRONT_ID: $MFA_PRODUCTION_CLOUDFRONT_DIST_ID
  script: !reference [.base-rollback, script]
  rules:
    - *release_high_environment_manual