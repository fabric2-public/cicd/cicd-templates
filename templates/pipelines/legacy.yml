include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/sonarcloud.yml
      - templates/npm.yml

stages:
  - test
  - analyze
  - sandbox-approve
  - production-approve
  - prepare
  - deploy
  - trigger-prepare
  - trigger-api


.rules:
  rules:
    - &main
      if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: always
    - &if-drcommerce
      if: '$STAGE == "uat01" && $CI_COMMIT_BRANCH == "drcommerce"'
    - &if-testing
      if: '($STAGE == "dev01" || $STAGE == "dev02" || $STAGE == "stg02" || $STAGE == "stg02pim2") && $CI_PIPELINE_SOURCE == "web"'
    - &only-if-trunk
      if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_COMMIT_BRANCH != "trimark-specifics"'
      when: never
    - &merge-request
      if: '"$CI_MERGE_REQUEST_IID"'
      when: always
    - &no-run-on-manual
      if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "trigger"'
      when: never    
    - &run-on-manual
      if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
    - &deploy-sandbox
      if: '$CI_PIPELINE_SOURCE == "web" && ($STAGE == "sandbox" || $STAGE == "uat01" || $STAGE == "demo" || ($STAGE == "dev01" && $CLIENTS_LIST_ARRAY != "bailey") || $STAGE == "poc01")'
      when: always
    - &deploy-production
      if: '$CI_PIPELINE_SOURCE == "web" && $STAGE == "prod01"'
      when: always
    - &run-on-trigger
      if: '$CI_PIPELINE_SOURCE == "trigger"'
      when: always


variables:
  API_NAME:
    description: Name of API
  CLIENTS_LIST_ARRAY:
    description: Name of client or comma-separated list of clients
  STAGE:
    description: Name of stage
  REGION:
    description: AWS region in which to deploy the API
    value: us-east-1

unit_test:
  variables:
    npm_cmd: $NPM_RUN_UNIT_TEST_CMD
    DD_API_KEY: $DD_API_KEY
    DD_INSIDE_CI: "true"
    DD_HOSTNAME: "none"
    DD_AGENT_HOST: "datadog-agent"
  services:
    - name: datadog/agent:latest
  stage: test
  extends: .npm-run-14    
  needs: []
  rules:
    - *no-run-on-manual
    - *merge-request
    - *main

sonarcloud:
  stage: analyze
  extends: .sonarcloud
  allow_failure: true
  needs: [unit_test]
  rules:
    - *no-run-on-manual
    - *merge-request
    - *main

approve-sandbox:
  stage: sandbox-approve
  environment:
    name: sandbox-approve
    action: prepare
  script:
    - echo $GITLAB_USER_NAME approves of this deployment.
  needs: []
  rules:
    - *only-if-trunk
    - <<: *deploy-sandbox
      when: manual

approve-production:
  stage: production-approve
  environment:
    name: production-approve
    action: prepare
  script:
    - echo $GITLAB_USER_NAME approves of this deployment.
  needs: []
  rules:
    - *only-if-trunk
    - <<: *deploy-production
      when: manual

generate-gitlab-ci:
  stage: prepare
  image: registry.gitlab.com/fabric2-public/cicd/cicd-templates/python-toolbox:v0.0.1
  variables:
    TEMPLATE_URL: https://gitlab.com/fabric2-public/cicd/cicd-templates/-/raw/main/.gitlab-ci/parallel_deployment.jsonnet  
  script: |
    set -x
    curl -O $TEMPLATE_URL
    pipeline_stage="deploy-$STAGE"
    case "$STAGE" in 
      "dev01")   pipeline_stage="deploy-DEV";;
      "dev02")   pipeline_stage="deploy-DEV";;
      "stg01")   pipeline_stage="deploy-QA";;
      "stg02")   pipeline_stage="deploy-QA";;
      "prod02")  pipeline_stage="deploy-INT-UAT";;
      "sandbox") pipeline_stage="deploy-SANDBOX";;
      "uat01")   pipeline_stage="deploy-SANDBOX";;
      "prod01")  pipeline_stage="deploy-PROD";;
    esac
    echo "pipeline_stage=$pipeline_stage"
    jsonnet -S \
      --ext-str stage="$STAGE" \
      --ext-str client_list="$CLIENTS_LIST_ARRAY" \
      --ext-str platform="$PLATFORM" \
      --ext-str api_list="$API_NAME" \
      --ext-str region="$REGION" \
      --ext-str function_set="$FUNCTION_SET" \
      --ext-str pipeline_stage="$pipeline_stage" \
      parallel_deployment.jsonnet >generated-config.yml
  artifacts:
    paths:
      - generated-config.yml
  needs: 
    - job: approve-sandbox
      optional: true
    - job: approve-production
      optional: true
  rules:
    - *if-testing
    - *if-drcommerce
    - *only-if-trunk
    - *run-on-manual

parallel-deploy:
  stage: deploy
  # image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate-gitlab-ci
    strategy: depend
  needs: [generate-gitlab-ci]
  rules:
    - *if-testing
    - *if-drcommerce
    - *only-if-trunk
    - *run-on-manual 
 
generate-parallel-API-gitlab-ci:
  stage: trigger-prepare
  image: registry.gitlab.com/fabric2-public/cicd/cicd-templates/python-toolbox:v0.0.1
  variables:
    TEMPLATE_URL: https://gitlab.com/fabric2-public/cicd/cicd-templates/-/raw/main/.gitlab-ci/parallel_deployment.jsonnet
    CLIENT: $TARGET_CLIENT 
    STAGE: $TARGET_STAGE 
    REGION: $TARGET_REGION
  script: |
    export REGION="${REGION:-$TARGET_REGION}"
    curl -O $TEMPLATE_URL
    pipeline_stage="deploy-$STAGE"
    case "$STAGE" in 
      "sandbox") pipeline_stage="SANDBOX";;
      "uat01")   pipeline_stage="SANDBOX";;
      "prod01")  pipeline_stage="PROD";;
    esac
    if [ -z "$API_ARRAY" ]; then
      echo "API_ARRAY must be set in inheriting template variables."
      exit 1
    fi
    if [ ! -z "$TARGET_API_NAME" ]; then
      echo "TARGET_API_NAME set, which overrides API_ARRAY"
      API_ARRAY=`echo $TARGET_API_NAME | sed 's/api-//'`
    fi
    MOD_API_ARRAY=`echo "$API_ARRAY" | awk '{$1=$1};1'`
    jsonnet -S \
      --ext-str stage="$STAGE" \
      --ext-str client_list="$CLIENT" \
      --ext-str platform="$PLATFORM" \
      --ext-str api_list="$MOD_API_ARRAY" \
      --ext-str region="$REGION" \
      --ext-str function_set="$FUNCTION_SET" \
      --ext-str pipeline_stage="$pipeline_stage" \
      parallel_deployment.jsonnet >parallel-api-d-config.yml
  artifacts:
    paths:
      - parallel-api-d-config.yml
  rules:
    - *run-on-trigger
    - when: never

parallel-API-deploy:
  stage: trigger-api
  trigger:
    include:
      - artifact: parallel-api-d-config.yml
        job: generate-parallel-API-gitlab-ci
    strategy: depend
  needs: [generate-parallel-API-gitlab-ci]
  rules:
    - *run-on-trigger
    - when: never

